package com.nbodev.apm.jmx;

import java.util.concurrent.ThreadLocalRandom;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Example of JMX metrics, those are refreshed with random values.
 * <p>
 * Each fixedRate seconds we refresh the values
 */
@Component
@ManagedResource(objectName = "com.nbodev:name=DemoJMX", description = "DemoJMX")
public class DemoJMX {
	private int demoLastInt;
	private double demoLastDouble;

	private static final Double MAX_DOUBLE = 500d;
	private static final Integer MAX_INTEGER = 500;

	public DemoJMX() {
		demoLastDouble = ThreadLocalRandom.current().nextDouble(MAX_DOUBLE);
		demoLastInt = ThreadLocalRandom.current().nextInt(0, MAX_INTEGER);
	}

	@Scheduled(fixedRate = 10000)
	public void generateRandomMetricsData() {
		demoLastDouble = ThreadLocalRandom.current().nextDouble(MAX_DOUBLE);
		demoLastInt = ThreadLocalRandom.current().nextInt(0, MAX_INTEGER);
	}

	// below manual jmx operations that can be used for test, you can trigger them from the jconsole
	@ManagedOperation
	public synchronized int genRandomInt() {
		demoLastInt = ThreadLocalRandom.current().nextInt(0, MAX_INTEGER);
		return demoLastInt;
	}

	@ManagedOperation
	public synchronized double genRandomDouble() {
		demoLastDouble = ThreadLocalRandom.current().nextDouble(MAX_DOUBLE);
		return demoLastDouble;
	}

	@ManagedAttribute(description = "demoLastInt")
	public synchronized int getDemoLastInt() {
		return demoLastInt;
	}

	@ManagedAttribute(description = "demoLastDouble")
	public synchronized double getDemoLastDouble() {
		return demoLastDouble;
	}

}
