package com.nbodev.apm.micrometer;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * Example of Micrometer metrics, those are refreshed with random values.
 * <p>
 * Each fixedRate seconds we refresh the values
 */
@Component
public class DemoMicrometer {

	// counter: keeps incrementing all the time
	private final Counter demoCounter;

	// gauge: up and down
	private final AtomicInteger demoGauge;

	public DemoMicrometer(MeterRegistry meterRegistry) {
		this.demoCounter = meterRegistry.counter("demo_counter");
		this.demoGauge = meterRegistry.gauge("demo_gauge", new AtomicInteger(0));
	}

	@Scheduled(fixedRate = 5000)
	public void generateRandomMetricsData() {
		demoGauge.set(ThreadLocalRandom.current().nextInt(0, 500));
		demoCounter.increment(ThreadLocalRandom.current().nextDouble(1, 10) * ThreadLocalRandom.current().nextInt(-1, 2));
	}

}
