package com.nbodev.apm.consul;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.agent.model.NewService;

/**
 * Spring configuration for service discovery with a Consul server.
 *
 * <p>
 * When the application starts, it automatically registers itself to Consul with its own address and health check path (i.e. the URL to the Prometheus JMX
 * Exporter).
 * <p>
 * By using this configuration, Prometheus does not need to know the port of the jmx exporter and relies on the service name that has been used to register to
 * Consul.
 * <p>
 * Prometheus relies on Consul to know what instances are up and running, and what is the path to get the metrics.
 * <p>
 * Prometheus will use consul_sd_configs with the service name as defined by the services when we registered them.
 * <p>
 * In short, this will be used by the Prometheus jmx exporter then it register itself to Consul but also can be used by any generic server that wants to make
 * itself visible through Consul in a dynamic way (even if the IP or port change).
 * <p>
 * The properties defined below are defined in the applicaton.yml file, some of them have already default values.
 *
 */
@Configuration
public class ConsulServiceRegisterConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsulServiceRegisterConfig.class);

	// consul
	@Value("${apm.consul.host:#{null}}")
	private String consulHost;

	@Value("${apm.consul.port:8500}")
	private Integer consulPort;

	// node having the service
	@Value("${apm.node.host:#{null}}")
	private String serviceHost;

	@Value("${apm.node.instance.name:#{null}}")
	private String instanceName;

	@Value("${apm.node.service.name}")
	private String serviceName;

	@Value("${apm.node.healthcheck.port:8080}")
	private Integer servicePort;

	@Value("${apm.node.healthcheck.path:metrics}")
	private String serviceHealthCheckPath;

	@Value("${apm.node.healthcheck.interval:15s}")
	private String serviceHealthCheckInterval;

	@Value("${apm.node.healthcheck.timeout:30s}")
	private String serviceHealthCheckTimeout;

	@Value("${apm.node.healthcheck.deregister.time:1s}")
	private String serviceHealthCheckDeregisterTime;

	/**
	 * Registers service to Consul
	 */
	@Bean
	public Void register() {
		LOGGER.info("Consul is reacheable on host[{}] and on port [{}].", consulHost, consulPort);

		try {
			LOGGER.info("About to register the service [{}] running on host[{}] and on port [{}] to Consul.",
					serviceName, serviceHost, servicePort);
			registerServiceToConsul();
			LOGGER.info("Registration of the service [{}] running on host[{}] and on port [{}] to Consul is successful.",
					serviceName, serviceHost, servicePort);
		} catch (final Exception e) {
			// Catch the exception in order to prevent the application from stopping, this service won't register to Consul
			LOGGER.error(
					"Failed to register the service [{}] running on host[{}] and on port [{}] to Consul, "
							+ "make sure you can reach Consul and your configuration is correct and restart.",
					serviceName, serviceHost, servicePort, e);
		}
		return null;
	}

	private void registerServiceToConsul() throws UnknownHostException, SocketException {

		ConsulClient client;

		try {
			client = new ConsulClient(this.consulHost, this.consulPort);
		} catch (final Exception e) {
			LOGGER.error("Failed to instantiate ConsulClient, please check the Consul host [{}] and port [{}].", consulHost, consulPort, e);
			throw new UnknownHostException("Consul host:" + this.consulHost + " - Consul port:" + this.consulPort);
		}

		if (this.serviceHost == null || this.serviceHost.length() == 0) {
			this.serviceHost = findHostIP();
			LOGGER.warn("No serviceHost provided, we will use local IP address [{}].", this.serviceHost);
		}

		final var serviceHostAndPort = this.serviceHost + ":" + this.servicePort;

		// default nodeInstanceName to serviceHostAndPort if not defined
		if (this.instanceName == null) {
			this.instanceName = serviceHostAndPort;
		}

		// setup new service
		final var service = new NewService();
		service.setId(this.instanceName);
		service.setName(this.serviceName);
		service.setAddress(this.serviceHost);
		service.setPort(this.servicePort);

		// setup associated health check
		final var serviceCheck = new NewService.Check();
		final var healthCheckUrl = String.format("http://%s/%s", serviceHostAndPort, this.serviceHealthCheckPath);
		serviceCheck.setHttp(healthCheckUrl);
		LOGGER.info("Consul Health Check URL: {}", healthCheckUrl);
		serviceCheck.setInterval(this.serviceHealthCheckInterval);
		serviceCheck.setTimeout(this.serviceHealthCheckTimeout);
		serviceCheck.setDeregisterCriticalServiceAfter(this.serviceHealthCheckDeregisterTime);
		service.setCheck(serviceCheck);

		// register
		client.agentServiceRegister(service);
	}

	private static String findHostIP() throws SocketException, UnknownHostException {
		final var interfaces = NetworkInterface.getNetworkInterfaces();

		while (interfaces.hasMoreElements()) {
			final var i = interfaces.nextElement();

			if (!i.isVirtual() && !i.isLoopback()) {
				final var inetAddresses = i.getInetAddresses();

				while (inetAddresses.hasMoreElements()) {
					final var addr = inetAddresses.nextElement();

					if (addr instanceof Inet4Address) {
						return addr.getHostAddress();
					}
				}
			}
		}
		return InetAddress.getLocalHost().getHostAddress();
	}

}
