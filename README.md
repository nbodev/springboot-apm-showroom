<h1 align="center">APM Showroom</h1>

### 📋 Introduction

- This project goal is to give an example of how Consul, Prometheus and Grafana can be used to monitor java applications dynamically.
- This java application is having JMX and Micrometer metrics.

### 📋 Versions

- Java 11
- Spring Boot 2.6.6
- Spring Cloud 2021.0.0
- Consul 1.11.1
- Prometheus 2.24.1
- Grafana 7.3.7

### 📦 Compile

- Run the following in order to compile the project `mvn clean install`.

### 💻 Start Consul, Prometheus and Grafana

- Install Docker.
- Clone `https://gitlab.com/nbodev/docker-apm-showroom`.
- Run the following:

```
cd docker-apm-showroom/consul-prometheus-grafana
docker-compose up
```
- Refer to the `consul-prometheus-grafana/README.md` for more details.

### 💻 Start an application instance, you can start many ones

- Run the following in order to start an instance:

```
java -javaagent:/workspace/springboot-apm-showroom/src/main/resources/jmx-exporter-config/jmx_prometheus_javaagent-0.16.1.jar=7771:/workspace/springboot-apm-showroom/src/main/resources/jmx-exporter-config/jmx-exporter-config.yml -Djmx.export.port=7771 -Dnode.name=node1  -Dserver.port=9991 -jar ./target/apm-showroom-0.0.1-SNAPSHOT.jar
```

Make sure that you change the following in case you start a second instance:
- JMX exporter port: `-javaagent:/workspace/springboot-apm-showroom/src/main/resources/jmx-exporter-config/jmx_prometheus_javaagent-0.16.1.jar=<jmx exporter port> -Djmx.export.port=<jmx exporter port>` 
- JMX exporter port is used twice, in the agent and in the `-Djmx.export.port` jvm arg. The latter is proper to this application and will be used later on to register the JMX exporter instance to Consul.
- node name: `-Dnode.name=<node name>`, this argument is proper to my application, it is used to distinguish the different instances.
- Spring Boot application port: `-Dserver.port=<Spring Boot application port>`, this is the classic Spring Boot port argument.

### 💻 Check the monitoring stack

- Go to Consul: `http://localhost:8500/`
- Go to Prometheus: `http://localhost:9092/`
- Go to Grafana: `http://localhost:3000/`

### 📋 Explanation

##### Step 1: metrics

- Metrics are defined in two classes: `DemoJMX` and `DemoMicrometer`.
- `DemoJMX` defines the metrics as MBeans (demoLastInt, demoLastDouble), you can see  them in the jconsole for instance.
- `DemoMicrometer` defines the metrics as Micrometer metrics (demoCounter, demoGauge), we expose them to Prometheus thanks to this section that is added in the `pom.xml`:

```		
<dependency>
	<groupId>io.micrometer</groupId>
	<artifactId>micrometer-registry-prometheus</artifactId>
</dependency>
```
- Note that those metrics (JMX and Micrometer) keep changing randomly every ... milliseconds, a method with this annotation `@Scheduled(fixedRate = ...)` is used in each class.

##### Step 2: register to Consul

###### Spring Cloud

- Spring Cloud is used in order to register to Consul, for that we need to add those dependencies in the `pom.xml`:

```
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>
	
	...
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
	...	
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-consul-discovery</artifactId>
		</dependency>
```
In the `application.yml` we have this configuration:

```
# enable all the actuator endpoints
management:
  endpoints:
    web:
      exposure:
        include: "*"
        
# spring cloud configuration, exposes the micrometer metrics and registers to consul
spring:
  application:
    name: my-service
  cloud:
    consul:
      host: localhost
      port: 8500
      discovery:
        # instance-id must not be empty, must start with letter, end with a letter or digit, and have as interior characters only letter, digits, and hyphen
        instance-id: ${spring.application.name}-${node.name}-micrometer-instance
        serviceName: ${spring.application.name}
        register: true
      retry: 
        initial-interval: 2000
        max-attempts: 10000
        max-interval: 4000
        multiplier: 1.1
```
- The first instance that will register to Consul thanks to Spring Cloud is the instance that will expose the Micrometer metrics. This instance is running on the port of Spring Boot (e.g. 9991), assuming I run my application with those jvm arguments:

```
java -javaagent:/workspace/springboot-apm-showroom/src/main/resources/jmx-exporter-config/jmx_prometheus_javaagent-0.16.1.jar=7771:/workspace/springboot-apm-showroom/src/main/resources/jmx-exporter-config/jmx-exporter-config.yml -Djmx.export.port=7771 -Dnode.name=node1  -Dserver.port=9991 -jar ./target/apm-showroom-0.0.1-SNAPSHOT.jar
```

- The instance name that will be seen on Consul side will be `my-service-node1-micrometer-instance` and the metrics are available at this url `http://localhost:9991/actuator/prometheus` (in Prometheus format).

- Note that we are setting up a retry configuration here in case Consul is unavailable, see the `retry` section in the `application.yml`, this requires the following to be added in your `pom.xml`:

```
		<!-- Spring Retry in order to retry the connection if Consul is unavailable -->
		<dependency>
			<groupId>org.springframework.retry</groupId>
			<artifactId>spring-retry</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>
```

###### Custom-made class ConsulServiceRegisterConfig

- Note that we also have the JMX exporter in the jvm arguments, this will also be registered as  another instance to Consul, this instance has this name my-service-node1-jmx-instance according to our configuration.
- The JMX metrics are available here `http://localhost:7771/actuator/prometheus`.
- This instance registers to Consul thanks to the custom-made class `ConsulServiceRegisterConfig` that uses Consul API. There is a configuration in the `application.yml` that applies to this class.

```
# bespoke consul registration of jmx exporter into consul, exposes the jmx metrics
apm:
  consul:
    host: ${spring.cloud.consul.host}
    port: ${spring.cloud.consul.port}
  node:
    healthcheck:
      port: ${jmx.export.port}
      path: actuator/prometheus
    service:
      name: ${spring.application.name}
    instance:
      name: ${spring.application.name}-${node.name}-jmx-instance
 ```

##### Step 3: Prometheus configuration

- Prometheus does not need to know the port of the JMX exporter and relies on the fact that the same service name `my-service` is used for the two instances (Micrometer and JMX exporter) when we register them to Consul.
- Prometheus will use consul_sd_configs with the `myservice` name as defined by the services when we registered them.
- See Prometheus configuration below, we focus on the service and scrape `/actuator/prometheus`.
- This is already the path of the Micrometer metrics (`http://localhost:9991/actuator/prometheus`) and the JMX exporter metrics (`http://localhost:7771/actuator/prometheus`).

```
scrape_configs:
# consul job
- job_name: 'consul'
  consul_sd_configs:
  - server: 'apm-showroom-consul-server:8500'
    services:
    - my-service
  metrics_path: '/actuator/prometheus'
  relabel_configs:
  - source_labels: [__meta_consul_service_id]
    regex: 'my-service-(.*)-(.*)-(.*)'
    replacement: 'my-service-$1'
    target_label: node
  - source_labels: [__meta_consul_service_id]
    target_label: instance
 ```

- Prometheus scrape all the instances related to the service `my-service` by resolving them dynamically. No need to know the ip address / hostname / port.
- You can see in the `prometheus.yml` configuration that we create a new label `node` thanks to the `relabel_config` this label will have the node name without the reference to the metrics type.
- Note that the `prometheus.yml` file is available in the `https://gitlab.com/nbodev/docker-apm-showroom` project under `consul-prometheus-grafana/prometheus/config/prometheus.yml`.
- e.g. the JMX instance will have this instance label `instance="my-service-node1-jmx-instance"`, the Micrometer one will have this one `instance="my-service-node1-micrometer-instance"`. <br/>
After relabeling, we create a new label `node` with this value `node="my-service-node1"`. This is achieved with this part of the  `prometheus.yml` configuration.

```
  relabel_configs:
  - source_labels: [__meta_consul_service_id]
    regex: 'my-service-(.*)-(.*)-(.*)'
    replacement: 'my-service-$1'
    target_label: node
```
- This will be used as a variable in Grafana, so we can filter on a particular node in order to get the related metrics.

##### Step 4: Grafana configuration

- Go to `http://localhost:3000/`.
- Login with `admin/admin`.
- Add the Prometheus data source, use `http://apm-showroom-prometheus:9090` as url.
- Import the dashboard located in `consul-prometheus-grafana/grafana/dashboard/APM Showroom.json` from the `https://gitlab.com/nbodev/docker-apm-showroom` project.
- This dashboard has a variable named `node` defined with the Prometheus up metrics and the Regex `/.*node="(.*)"/` , this node that you see in the dashboard Regex is the one that has been created with the Prometheus relabeling.

### 📡 Links

Detailed version of this project
- https://nbodev.medium.com/application-performance-monitoring-monitor-dynamically-java-applications-with-consul-prometheus-2618fa712bfd

Micrometer
- https://micrometer.io/docs/concepts
- https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready-metrics
- https://github.com/prometheus/jmx_exporter

Spring Cloud
- https://docs.spring.io/spring-cloud-consul/docs/current/reference/html/index.html
- https://cloud.spring.io/spring-cloud-consul/reference/html/appendix.html

Consul
- https://learn.hashicorp.com/tutorials/consul/docker-container-agents
- https://www.consul.io/docs/agent
- https://github.com/Ecwid/consul-api

Prometheus
- https://prometheus.io/docs/prometheus/latest/querying/basics/
- https://prometheus.io/docs/prometheus/latest/configuration/configuration/
- https://www.fosstechnix.com/prometheus-promql-tutorial-with-examples/

Grafana
- https://grafana.com/docs/grafana/latest/datasources/prometheus/
- https://grafana.com/docs/grafana/latest/dashboards/export-import/
- https://grafana.com/docs/grafana/latest/variables/

